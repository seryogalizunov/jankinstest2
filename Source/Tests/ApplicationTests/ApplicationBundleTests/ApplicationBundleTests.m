//
//  ApplicationBundleTests.m
//  NIXProject
//
//  Created by Nezhelskoy Iliya on 11/15/14.
//  Copyright (c) 2014 NIX. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>

@interface ApplicationBundleTests : XCTestCase

@property(nonatomic, weak) NSBundle *currentBundle;

@end

@implementation ApplicationBundleTests

#pragma mark -

- (void)setUp
{
    [super setUp];
    
    [self setCurrentBundle:[NSBundle bundleForClass:[self class]]];
}

- (void)tearDown
{
    [self setCurrentBundle:nil];
    
    [super tearDown];
}

#pragma mark -

- (void)testThatLogicBundleLoaded
{
    static NSString *const expectedExecutable = @"ApplicationTests";
    
    XCTAssertEqualObjects([[self currentBundle] objectForInfoDictionaryKey:@"CFBundleExecutable"], expectedExecutable);
    XCTAssertTrue([[self currentBundle] isLoaded]);
}

@end
